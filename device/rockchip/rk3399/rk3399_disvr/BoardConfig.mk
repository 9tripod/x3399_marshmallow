include device/rockchip/rk3399/BoardConfig.mk

# Sensors
BOARD_SENSOR_ST := false
BOARD_SENSOR_MPU := false
BOARD_SENSOR_MPU_VR := true

# set true if you use dual mipi screen.
BOARD_USE_DUAL_MIPI := true

TARGET_BOARD_PLATFORM_PRODUCT := vr
