package com.android.systemui.bluetooth;
import com.android.systemui.SystemUI;
import android.content.BroadcastReceiver;
import android.bluetooth.BluetoothDevice;
import android.widget.Button;
import android.app.AlertDialog;
import android.widget.TextView;
import android.view.WindowManager;
import android.util.Log;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Context;
import android.view.View;
import android.text.InputType;
import android.widget.EditText;
import java.io.UnsupportedEncodingException;
import android.net.Uri;
//import android.text.method.PasswordTransformationMethod;
import android.content.ContentValues;
import android.widget.Toast;
import java.text.DecimalFormat;
import com.android.systemui.R;
/** *
    This is a SystemUI to show all Bluetooth Message for VR devices
    Created by tim.zhou on 2016/12/7.

 */
public class BluetoothUI extends SystemUI
{
	static final String TAG = "BluetoothUI";
	private TextView mBluetoothDeviceTv;
    private TextView mBluetoothDeviceTvPin;
	private TextView mBluetoothPinCodeTv;
    private TextView mBluetoothFileTitleTv;
    private TextView mBluetoothFileNameTv;
    private TextView mBluetoothFileSizeTv;
    private TextView mBluetoothFileHintTv;
    private EditText mBluetoothPincodeET;
    private Button mBluetoothOkButton;
    private Button mBluetoothOkButtonPin;
    private Button mBluetoothFileOkButton;
    private Button mBluetoothCancelButton;
    private Button mBluetoothCancelButtonPin;
    private Button mBluetoothFileCancelButton;

    private AlertDialog mBluetoothDialog;
    private AlertDialog mBluetoothDialogPin;
    private AlertDialog mBluetoothFileDialog;
    private boolean mWithCode;
    private PairRequestReceiver mPairReceiver;
    private BluetoothDevice btDevice;
    private int type;
    private Uri uri;


    public void start() {
    IntentFilter filter = new IntentFilter();
    filter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
    filter.addAction("android.bluetooth.device.action.PAIRING_CANCEL");
    filter.addAction("com.android.bluetooth.oppFileCome");
    filter.addAction("com.android.bluetooth.oppComplete");
    mPairReceiver=new PairRequestReceiver();
    mContext.registerReceiver(mPairReceiver, filter);
    }

public class PairRequestReceiver extends BroadcastReceiver {
    private static final String TAG = "PairRequestReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
      
        String action = intent.getAction();
        if ("android.bluetooth.device.action.PAIRING_REQUEST".equals(action)) {
             btDevice =intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
             type = intent.getIntExtra(BluetoothDevice.EXTRA_PAIRING_VARIANT,
                    BluetoothDevice.ERROR);
            switch (type) {
                case BluetoothDevice.PAIRING_VARIANT_PASSKEY_CONFIRMATION:
                    // 创建配对码确认View
                    int pairingKey = intent.getIntExtra(BluetoothDevice.EXTRA_PAIRING_KEY,
                            BluetoothDevice.ERROR);
                    if (pairingKey == BluetoothDevice.ERROR) {
                        Log.e(TAG, "Invalid Confirmation Passkey received, not showing any dialog");
                        return;
                    }
                    View v =  View.inflate(mContext,R.layout.vr_bluetooth, null);  
                    mBluetoothDeviceTv=(TextView)v.findViewById(R.id.vr_bt_devices);
                    mBluetoothPinCodeTv=(TextView)v.findViewById(R.id.vr_bt_pin);
                    mBluetoothOkButton=(Button)v.findViewById(R.id.bluetooth_ok);
                    mBluetoothCancelButton=(Button)v.findViewById(R.id.bluetooth_cancel);
                    mBluetoothDeviceTv.setText(mContext.getString(R.string.vr_bluetooth_device)+"  "+btDevice.getName());
                    mBluetoothPinCodeTv.setText(mContext.getString(R.string.vr_bluetooth_pin)+"  "+pairingKey);
                    mBluetoothOkButton.setText(mContext.getString(R.string.vr_bluetooth_ok));
                    mBluetoothCancelButton.setText(mContext.getString(R.string.vr_bluetooth_cancel));
                    AlertDialog.Builder b = new AlertDialog.Builder(mContext); 
                    b.setCancelable(true); 
                    b.setView(v);	
                    mBluetoothDialog = b.create(); 
		  			mBluetoothDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);  
		  			mBluetoothDialog.show();
                    mBluetoothOkButton.setOnClickListener(new View.OnClickListener() {
                    	@Override
                    	public void onClick(View view) {
                            if (btDevice!=null)
                            {
                                btDevice.setPairingConfirmation(true);
                            }
                    		if(mBluetoothDialog!=null){
                    			mBluetoothDialog.dismiss();
                    		}
                    	}
                    });
                    mBluetoothCancelButton.setOnClickListener(new View.OnClickListener() {
                    	@Override
                    	public void onClick(View view) {
                             if (btDevice!=null)
                            {
                               btDevice.setPairingConfirmation(false);
                            }
                    		if(mBluetoothDialog!=null){
                    			mBluetoothDialog.dismiss();
                    		}
                    	}
                    });
   					mWithCode=false;

                    break;
                case BluetoothDevice.PAIRING_VARIANT_PIN:
                case BluetoothDevice.PAIRING_VARIANT_PASSKEY:  
                    View v_pin = View.inflate(mContext,R.layout.vr_bluetooth_pincode, null);  
                    mBluetoothDeviceTvPin=(TextView)v_pin.findViewById(R.id.vr_bt_devices_pincode);
                    mBluetoothPincodeET=(EditText)v_pin.findViewById(R.id.vr_bt_pincode_et);
                    mBluetoothPincodeET.setInputType(InputType.TYPE_CLASS_NUMBER); 
                  //  mBluetoothPincodeET.setTransformationMethod(PasswordTransformationMethod.getInstance()); 
                    mBluetoothOkButtonPin=(Button)v_pin.findViewById(R.id.bluetooth_ok_pincode);
                    mBluetoothCancelButtonPin=(Button)v_pin.findViewById(R.id.bluetooth_cancel_pincode);
                    mBluetoothDeviceTvPin.setText(mContext.getString(R.string.vr_bluetooth_device)+"  "+btDevice.getName());
                    mBluetoothOkButtonPin.setText(mContext.getString(R.string.vr_bluetooth_ok));
                    mBluetoothCancelButtonPin.setText(mContext.getString(R.string.vr_bluetooth_cancel));
                    AlertDialog.Builder b_pin = new AlertDialog.Builder(mContext); 
                    b_pin.setCancelable(true); 
                    b_pin.setView(v_pin);   
                    mBluetoothDialogPin = b_pin.create(); 
                    mBluetoothDialogPin.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);  
                    mBluetoothDialogPin.show();
                    mBluetoothOkButtonPin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (btDevice!=null&&mBluetoothPincodeET.getText().toString()!=null)
                            {
                                byte[] pinBytes = convertPinToBytes(mBluetoothPincodeET.getText().toString());
                                if (pinBytes == null) {
                                }else{
                                     btDevice.setPin(pinBytes);
                                }
                            }
                            if(mBluetoothDialogPin!=null){
                                mBluetoothDialogPin.dismiss();
                            }
                        }
                    });
                    mBluetoothCancelButtonPin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                             if (btDevice!=null)
                            {
                               btDevice.cancelPairingUserInput();
                            }
                            if(mBluetoothDialogPin!=null){
                                mBluetoothDialogPin.dismiss();
                            }
                        }
                    });
                    mWithCode=true;
                    // 创建用户输入密码的View并且显示
                    break;
            }
        } else if ("android.bluetooth.device.action.PAIRING_CANCEL".equals(action)) {
            if (mWithCode) {
             if(mBluetoothDialogPin!=null){
                    mBluetoothDialogPin.dismiss();
                }
            }else{

            	if(mBluetoothDialog!=null){
            		mBluetoothDialog.dismiss();
            	}

            }
           
            
        }else if("com.android.bluetooth.oppFileCome".equals(action)){
            if (uri!=null&&uri.getPath().equals(((Uri)intent.getParcelableExtra("uri")).getPath())) {
                return;
            }
             uri = intent.getParcelableExtra("uri");
              Log.d("zytzyttest","uri:"+uri);
            String mDeviceName = intent.getStringExtra("deviceName");
            String mFileName = intent.getStringExtra("fileName");
            Long mFileSize = intent.getLongExtra("fileSize", 0);
            Log.d("zytzyttest","Size"+mFileSize);
            double mbsize = (double)mFileSize/(1024*1024);
            DecimalFormat df = new DecimalFormat(".000");
            String sizeString = df.format(mbsize);
            View v_file = View.inflate(mContext,R.layout.vr_bluetooth_filecomfirm, null);
            mBluetoothFileTitleTv=(TextView)v_file.findViewById(R.id.vr_bt_file_title);
            mBluetoothFileNameTv=(TextView)v_file.findViewById(R.id.vr_bt_file_name);
            mBluetoothFileSizeTv=(TextView)v_file.findViewById(R.id.vr_bt_file_size);
            mBluetoothFileHintTv=(TextView)v_file.findViewById(R.id.vr_bt_file_hint);
            mBluetoothFileOkButton=(Button)v_file.findViewById(R.id.bluetooth_ok_file);
            mBluetoothFileCancelButton=(Button)v_file.findViewById(R.id.bluetooth_cancel_file);
            mBluetoothFileTitleTv.setText(mContext.getString(R.string.vr_bluetooth_file_reciver_title)+"　"+mDeviceName);
            mBluetoothFileNameTv.setText(mContext.getString(R.string.vr_bluetooth_file_name)+"  "+mFileName);
            mBluetoothFileSizeTv.setText(mContext.getString(R.string.vr_bluetooth_file_size)+"  "+sizeString+"MB");
            mBluetoothFileHintTv.setText(mContext.getString(R.string.vr_bluetooth_file_hint));
            mBluetoothFileOkButton.setText(mContext.getString(R.string.vr_bluetooth_ok));
            mBluetoothFileCancelButton.setText(mContext.getString(R.string.vr_bluetooth_cancel));
            AlertDialog.Builder b_file = new AlertDialog.Builder(mContext); 
                    b_file.setCancelable(true); 
                    b_file.setView(v_file); 
            mBluetoothFileDialog = b_file.create();    
            mBluetoothFileDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT); 
            mBluetoothFileDialog.show();  

            mBluetoothFileOkButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                         
                         ContentValues updateValues = new ContentValues();
                        updateValues.put("confirm", 1);
                        mContext.getContentResolver().update(uri, updateValues, null, null);
                            if(mBluetoothFileDialog!=null){
                                mBluetoothFileDialog.dismiss();
                            }
                        }
                    });
            mBluetoothFileCancelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                         ContentValues updateValues = new ContentValues();
                         updateValues.put("confirm", 3);
                         mContext.getContentResolver().update(uri, updateValues, null, null);

                            if(mBluetoothFileDialog!=null){
                                mBluetoothFileDialog.dismiss();
                            }
                        }
                    });

        }else if("com.android.bluetooth.oppComplete".equals(action)){
           Toast.makeText(mContext,"Sucess",Toast.LENGTH_SHORT).show();
    }
        }
}

 public byte[] convertPinToBytes(String pin) {
        if (pin == null) {
            return null;
        }
        byte[] pinBytes;
        try {
            pinBytes = pin.getBytes("UTF-8");
        } catch (UnsupportedEncodingException uee) {
            // this should not happen
            return null;
        }
        if (pinBytes.length <= 0 || pinBytes.length > 16) {
            return null;
        }
        return pinBytes;
    }

}
