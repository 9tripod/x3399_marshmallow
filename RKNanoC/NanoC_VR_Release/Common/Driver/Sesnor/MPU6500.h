/*************************************************************
 *Copyright (C) 2011 ROCK-CHIP FUZHOU. All Rights Reserved.
 *
 *File: MPU6500.h
 *Desc:

 *Author: wrm
 *Date: 16-04-24
 *Notes:
 *$Log: $
 *************************************************************/


#ifndef _MPU6500_H
#define _MPU6500_H

extern int32 MPU6500_Init(void);
extern int32 MPU6500_Read(uint32 reg_addr, uint8* data, uint32 size);
extern int32 MPU6500_Write(uint32 reg_addr, uint8 data);
extern int32 MPU6500_Read_Data(int16 *d_accel, uint8 r_accel, int16 *d_gyro, uint8 r_gyro);
extern float MPU6500_Read_Temperature(uint8 *tempeture);
extern int32 MPU6500_AccelEnable(int enable);
extern int32 MPU6500_GyroEnable(int enable);
#endif

