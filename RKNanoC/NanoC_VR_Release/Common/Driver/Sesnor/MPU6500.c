/*************************************************************
 *Copyright (C) 2011 ROCK-CHIP FUZHOU. All Rights Reserved.
 *
 *File: MPU6500.c
 *Desc:

 *Author: wrm
 *Date: 16-04-24
 *Notes:
 *$Log: $
 *************************************************************/
#include "MDconfig.h"


#include "Gpio.h"
#include "Hw_spi.h"
#include "ModuleOverlay.h"

//Register Map
#define SELF_TEST_X_GYRO_REG        0x00
#define SELF_TEST_Y_GYRO_REG        0x01
#define SELF_TEST_Z_GYRO_REG        0x02

#define SELF_TEST_X_ACCEL_REG       0x0D
#define SELF_TEST_Y_ACCEL_REG       0x0E
#define SELF_TEST_Z_ACCEL_REG       0x0F

#define XG_OFFSET_H_REG             0x13
#define XG_OFFSET_L_REG             0x14
#define YG_OFFSET_H_REG             0x15
#define YG_OFFSET_L_REG             0x16
#define ZG_OFFSET_H_REG             0x17
#define ZG_OFFSET_L_REG             0x18

#define SMPLRT_DIV_REG              0x19

#define CONFIG_REG                  0x1A
#define GYRO_CONFIG_REG             0x1B
#define ACCEL_CONFIG_REG            0x1C
#define ACCEL_CONFIG2_REG           0x1D

#define LP_ACCEL_ODR_REG            0x1E
#define WOM_THR_REG                 0x1F

#define FIFO_EN_REG                 0x23
#define I2C_MST_CTRL_REG            0x24
#define I2C_SLV0_ADDR_REG           0x25
#define I2C_SLV0_REG_REG            0x26
#define I2C_SLV0_CTRL_REG           0x27
#define I2C_SLV1_ADDR_REG           0x28
#define I2C_SLV1_REG_REG            0x29
#define I2C_SLV1_CTRL_REG           0x2A
#define I2C_SLV2_ADDR_REG           0x2B
#define I2C_SLV2_REG_REG            0x2C
#define I2C_SLV2_CTRL_REG           0x2D
#define I2C_SLV3_ADDR_REG           0x2E
#define I2C_SLV3_REG_REG            0x2F
#define I2C_SLV3_CTRL_REG           0x30
#define I2C_SLV4_ADDR_REG           0x31
#define I2C_SLV4_REG_REG            0x32
#define I2C_SLV4_DO_REG             0x33
#define I2C_SLV4_CTRL_REG           0x34
#define I2C_SLV4_DI_REG             0x35
#define I2C_MST_STATUS_REG          0x36

#define INT_PIN_CFG_REG             0x37
#define INT_ENABLE_REG              0x38
#define INT_STATUS_REG              0x3A

#define ACCEL_XOUT_H_REG            0x3B
#define ACCEL_XOUT_L_REG            0x3C
#define ACCEL_YOUT_H_REG            0x3D
#define ACCEL_YOUT_L_REG            0x3E
#define ACCEL_ZOUT_H_REG            0x3F
#define ACCEL_ZOUT_L_REG            0x40

#define TEMP_OUT_H_REG              0x41
#define TEMP_OUT_L_REG              0x42
#define GYRO_XOUT_H_REG             0x43
#define GYRO_XOUT_L_REG             0x44
#define GYRO_YOUT_H_REG             0x45
#define GYRO_YOUT_L_REG             0x46
#define GYRO_ZOUT_H_REG             0x47
#define GYRO_ZOUT_L_REG             0x48

#define EXT_SENS_DATA_00_REG        0x49
#define EXT_SENS_DATA_01_REG        0x4A
#define EXT_SENS_DATA_02_REG        0x4B
#define EXT_SENS_DATA_03_REG        0x4C
#define EXT_SENS_DATA_04_REG        0x4D
#define EXT_SENS_DATA_05_REG        0x4E
#define EXT_SENS_DATA_06_REG        0x4F
#define EXT_SENS_DATA_07_REG        0x50
#define EXT_SENS_DATA_08_REG        0x51
#define EXT_SENS_DATA_09_REG        0x52
#define EXT_SENS_DATA_10_REG        0x53
#define EXT_SENS_DATA_11_REG        0x54
#define EXT_SENS_DATA_12_REG        0x55
#define EXT_SENS_DATA_13_REG        0x56
#define EXT_SENS_DATA_14_REG        0x57
#define EXT_SENS_DATA_15_REG        0x58
#define EXT_SENS_DATA_16_REG        0x59
#define EXT_SENS_DATA_17_REG        0x5A
#define EXT_SENS_DATA_18_REG        0x5B
#define EXT_SENS_DATA_19_REG        0x5C
#define EXT_SENS_DATA_20_REG        0x5D
#define EXT_SENS_DATA_21_REG        0x5E
#define EXT_SENS_DATA_22_REG        0x5F
#define EXT_SENS_DATA_23_REG        0x60

#define I2C_SLV0_DO_REG             0x63
#define I2C_SLV1_DO_REG             0x64
#define I2C_SLV2_DO_REG             0x65
#define I2C_SLV3_DO_REG             0x66
#define I2C_MST_DELAY_CTRL_REG      0x67
#define SIGNAL_PATH_RESET_REG       0x68
#define ACCEL_INTEL_CTRL_REG        0x69
#define USER_CTRL_REG               0x6A

#define PWR_MGMT_1_REG              0x6B
#define PWR_MGMT_2_REG              0x6C

#define FIFO_COUNT_H_REG            0x72
#define FIFO_COUNT_L_REG            0x73
#define FIFO_R_W_REG                0x74
#define WHO_AM_I_REG                0x75

#define XA_OFFSET_H_REG             0x77
#define XA_OFFSET_L_REG             0x78
#define YA_OFFSET_H_REG             0x7A
#define YA_OFFSET_L_REG             0x7B
#define ZA_OFFSET_H_REG             0x7D
#define ZA_OFFSET_L_REG             0x7E

//Normal #define
#define SPI_PORT0                   0
#define SPI_PORT1                   1

#define MPU6500_ID_REG              0x70

#define SPI_CTRL_CONFIG             (SPI_MASTER_MODE|MOTOROLA_SPI|RXD_SAMPLE_NO_DELAY|APB_BYTE_WR|MSB_FBIT|LITTLE_ENDIAN_MODE|CS_2_SCLK_OUT_1_CK|CS_KEEP_LOW|SERIAL_CLOCK_POLARITY_HIGH|SERIAL_CLOCK_PHASE_START|DATA_FRAME_8BIT)


/* Sensitivity Scale Factor */
#define MPU6050_ACCEL_SCALE_SHIFT_2G	4
#define MPU6050_GYRO_SCALE_SHIFT_FS0	3

static uint8  MPU6500_SPICsn;


/*
Name:       MPU6500_Write
Desc:
Param:
Return:
Global:
Note:
Author:
Log:
*/
_ATTR_FLASH_CODE_
int32 MPU6500_Write(uint32 reg_addr, uint8 data)
{
    uint32 cmd[4]={0};
    uint32 data_tmp = 0;
    int time = 0;


    SPICtl->SPI_CTRLR0 = SPI_CTRL_CONFIG| TRANSMIT_RECEIVE;
    SPICtl->SPI_ENR = 1;
    SPICtl->SPI_SER = MPU6500_SPICsn;
    Gpio_SetPinLevel(GPIOPortA_Pin1, GPIO_LOW);
    //Gpio_SetPinLevel(GPIOPortC_Pin1, GPIO_LOW);

    cmd[0] = reg_addr;//写数据的地址
    SPICtl->SPI_TXDR[0] = cmd[0]; //*cmd++;

    cmd[1] = data;//要写的数据

    for(time=0; time<100; time++)  //现在SPI Flash速度是12Mb，发一个数据回来一个数据，因此需要经过8bit的时间，也就是83.34ns*8=666.67ns，timeout比1us大就行
    {
        if ((SPICtl->SPI_SR & RECEIVE_FIFO_EMPTY) != RECEIVE_FIFO_EMPTY)
        {
            data_tmp = SPICtl->SPI_RXDR[0];
            break;
        }
    }
    if (time > 100)
    {
        printf ("timeout\n");
    }

    #if 0
    SPICtl->SPI_ENR = 0;
    SPICtl->SPI_SER = 0;//MPU6500_SPICsn;
    //Gpio_SetPinLevel(GPIOPortA_Pin1, GPIO_HIGH);
    Gpio_SetPinLevel(GPIOPortC_Pin1, GPIO_HIGH);
    DelayUs(1);               //cs# high time > 100ns

    SPICtl->SPI_CTRLR0 = SPI_CTRL_CONFIG| TRANSMIT_RECEIVE;
    SPICtl->SPI_ENR = 1;
    SPICtl->SPI_SER = MPU6500_SPICsn;
    //Gpio_SetPinLevel(GPIOPortA_Pin1, GPIO_LOW);
    Gpio_SetPinLevel(GPIOPortC_Pin1, GPIO_LOW);
    #endif

    SPICtl->SPI_TXDR[0] = cmd[1]; //*cmd++;
    //printf ("Write 0x%x to 0x%x\n",cmd[1],cmd[0]);
    for(time=0; time<100; time++)  //现在SPI Flash速度是12Mb，发一个数据回来一个数据，因此需要经过8bit的时间，也就是83.34ns*8=666.67ns，timeout比1us大就行
    {
        if ((SPICtl->SPI_SR & RECEIVE_FIFO_EMPTY) != RECEIVE_FIFO_EMPTY)
        {
            data_tmp = SPICtl->SPI_RXDR[0];
            break;
        }
    }
    if (time > 100)
    {
        printf ("timeout\n");
    }

    SPICtl->SPI_ENR = 0;
    SPICtl->SPI_SER = 0;
    Gpio_SetPinLevel(GPIOPortA_Pin1, GPIO_HIGH);
    //Gpio_SetPinLevel(GPIOPortC_Pin1, GPIO_HIGH);
    DelayUs(1);               //cs# high time > 100ns

    return OK;
}


/*
Name:       MPU6500_Read
Desc:
Param:
Return:
Global:
Note:
Author:
Log:
*/
_ATTR_FLASH_CODE_
int32 MPU6500_Read(uint32 reg_addr, uint8* data, uint32 size)
{
    int32 ret=OK;
    uint32 time;
    uint32 cmd[4]={0};
    uint32 DummySize = 0,CmdLen = 1;
    uint32 DummyLen = 0, DataLen;
    uint32 data_tmp=0;

    SPICtl->SPI_CTRLR0 = SPI_CTRL_CONFIG| TRANSMIT_RECEIVE;
    SPICtl->SPI_ENR = 1;
    SPICtl->SPI_SER = MPU6500_SPICsn;
    Gpio_SetPinLevel(GPIOPortA_Pin1, GPIO_LOW);
    //Gpio_SetPinLevel(GPIOPortC_Pin1, GPIO_LOW);

    cmd[0] = reg_addr;//读数据的地址

    /*采用了收发模式, 发送命令的同时会接受数据进来, 所以要过滤掉CmdLen长度的数据
     DummySize 是命令发送完成后, 数据没有马上出来, 需要等待的无效的数据长度,
     一般SPINOR是不需要等待的, 所以DummySize=0*/
    DummyLen = (size > 0)? (DummySize+CmdLen):CmdLen;
    DataLen = size + DummyLen;
    while (DataLen)
    {
        if (CmdLen)
        {
            SPICtl->SPI_TXDR[0] = cmd[0]; //*cmd++;
            CmdLen--;
        }
        else
        {
            SPICtl->SPI_TXDR[0] = 0xFF;     //send clock
        }
        for(time=0; time<100; time++)  //现在SPI Flash速度是12Mb，发一个数据回来一个数据，因此需要经过8bit的时间，也就是83.34ns*8=666.67ns，timeout比1us大就行
        {
            if ((SPICtl->SPI_SR & RECEIVE_FIFO_EMPTY) != RECEIVE_FIFO_EMPTY)
            {
                if (DummyLen > 0)   //发送完命令后, 可能要等几个时钟,数据才出来
                {
                    data_tmp = SPICtl->SPI_RXDR[0];
                    *data = (uint8)(data_tmp & 0xFF);
                    DummyLen--;
                }
                else
                {
                    data_tmp = SPICtl->SPI_RXDR[0];
                    //printf ("--Valid data_tmp=0x%x\n",data_tmp);
                    *data++ = (uint8)(data_tmp & 0xFF);
                }
                break;
            }
            //DelayUs(1);
            //ASMDelay(1);
        }

        if (time>=100)
        {
            printf ("SPI  timeout\n");
            ret = ERROR;
            break;
        }

        DataLen--;
    }

    SPICtl->SPI_ENR = 0;
    SPICtl->SPI_SER = 0;//MPU6500_SPICsn;
    Gpio_SetPinLevel(GPIOPortA_Pin1, GPIO_HIGH);
    //Gpio_SetPinLevel(GPIOPortC_Pin1, GPIO_HIGH);
    DelayUs(1);               //cs# high time > 100ns

    return OK;
}

/*
Name:       MPU6500_AccelEnable
Desc:
Param:
Return:
Global:
Note:
Author:
Log:
*/
_ATTR_FLASH_CODE_
int32 MPU6500_AccelEnable(int enable)
{
    uint8 pwr_mgmt;
    if (enable)
    {
        MPU6500_Read(PWR_MGMT_2_REG|0x80, &pwr_mgmt, 1);
        pwr_mgmt = pwr_mgmt & 0xC7; //使能加速度计
        MPU6500_Write(PWR_MGMT_2_REG,pwr_mgmt);
    }
    else
    {
        MPU6500_Read(PWR_MGMT_2_REG|0x80, &pwr_mgmt, 1);
        pwr_mgmt = pwr_mgmt | 0x38; //使不能加速度计
        MPU6500_Write(PWR_MGMT_2_REG,pwr_mgmt);
    }

    return enable;
}

/*
Name:       MPU6500_GyroEnable
Desc:
Param:
Return:
Global:
Note:
Author:
Log:
*/
_ATTR_FLASH_CODE_
int32 MPU6500_GyroEnable(int enable)
{
    uint8 pwr_mgmt;
    if (enable)
    {
        MPU6500_Read(PWR_MGMT_2_REG|0x80, &pwr_mgmt, 1);
        pwr_mgmt = pwr_mgmt & 0xF8; //使能陀螺仪
        MPU6500_Write(PWR_MGMT_2_REG,pwr_mgmt);
    }
    else
    {
        MPU6500_Read(PWR_MGMT_2_REG|0x80, &pwr_mgmt, 1);
        pwr_mgmt = pwr_mgmt | 0x07;//使不能陀螺仪
        MPU6500_Write(PWR_MGMT_2_REG,pwr_mgmt);
    }

    return enable;
}

/*
Name:       MPU6500_Spi_Init
Desc:
Param:
Return:
Global:
Note:
Author:
Log:
*/
_ATTR_FLASH_CODE_
static void MPU6500_Spi_Init(uint8 port, uint8 spidev_cs)
{
    if  (port == 0)
    {
        #if 1 //若一开始SPI0没有初始化，就执行注释的步骤
        //SPI CTRL 选择24M 晶振 不分频输入
        CRUReg->CRU_CLKSEL_SPI_CON = 1<<0 | 0<<1 | 1<<16 | 63<<17;
        //init spi
        SPICtl->SPI_BAUDR = 2;     //sclk_out = 24M/2 = 12MHz
        SPICtl->SPI_CTRLR0 = (SPI_MASTER_MODE | TRANSMIT_RECEIVE | MOTOROLA_SPI | RXD_SAMPLE_NO_DELAY | APB_BYTE_WR
                                | MSB_FBIT | LITTLE_ENDIAN_MODE | CS_2_SCLK_OUT_1_CK | CS_KEEP_LOW | SERIAL_CLOCK_POLARITY_HIGH
                                | SERIAL_CLOCK_PHASE_START | DATA_FRAME_8BIT); // 8bit data frame size, CPOL=1,CPHA=1
        //CPOL=1,CPHA=1:空闲为高电平，第二个跳变沿采样

        //先设完spi的再设iomux，否则SPI-CLK会有一个低脉冲
        #endif

        switch (spidev_cs)
        {
            case 1: //Spinor flash
                break;
            case 2: //Sensor MPU6500
                //spi_rxd_p0, spi_txd_p0, spi_clk_p0, spi_csn0
                GRFReg->GPIO0A_IOMUX = 2<<6 | 2<<8 | 2<<10 | 2<<12 | 3<<22| 3<<24| 3<<26 | 3<<28;

                #if 1
                GpioMuxSet(GPIOPortA_Pin6,IOMUX_GPIOA6_IO);
                //拉低SPI Flash0 CS片选有效
                Gpio_SetPinDirection(GPIOPortA_Pin6,GPIO_OUT);
                Gpio_SetPinLevel(GPIOPortA_Pin6, GPIO_HIGH);//SPI0 Flash0_CS

                //GpioMuxSet(GPIOPortC_Pin1,IOMUX_GPIOC1_IO);
                //Gpio_SetPinDirection(GPIOPortC_Pin1,GPIO_OUT);
                //Gpio_SetPinLevel(GPIOPortC_Pin1, GPIO_HIGH);//拉高CS，MPU6500无法通信

                //使用GPIOA[1]做MPU6500的CS
                GpioMuxSet(GPIOPortA_Pin1,IOMUX_GPIOA1_IO);
                Gpio_SetPinDirection(GPIOPortA_Pin1,GPIO_OUT);
                Gpio_SetPinLevel(GPIOPortA_Pin1, GPIO_HIGH);//拉高CS，MPU6500无法通信
                #endif

                //spi1 切到gpio避免影响到 spi0 接口
                GRFReg->GPIO0B_IOMUX = 0<<2 | 0<<4 | 0<<6 | 0<<8 | 3<<18| 3<<20| 3<<22 | 3<<24;

                GRFReg->IOMUX_CON1 = 0<<1 | 1<<17; //spi_xxx_p0 available for spi 此设置只影响miso


                break;
            default:
                break;
        }
        MPU6500_SPICsn = 1;
    }

    if  (port == 1)
    {
        //SPI CTRL 选择24M 晶振 不分频输入
        CRUReg->CRU_CLKSEL_SPI_CON = 1<<0 | 0<<1 | 1<<16 | 63<<17;
        //init spi
        SPICtl->SPI_BAUDR = 2;     //sclk_out = 24M/2 = 12MHz
        SPICtl->SPI_CTRLR0 = (SPI_MASTER_MODE | TRANSMIT_RECEIVE | MOTOROLA_SPI | RXD_SAMPLE_NO_DELAY | APB_BYTE_WR
                                | MSB_FBIT | LITTLE_ENDIAN_MODE | CS_2_SCLK_OUT_1_CK | CS_KEEP_LOW | SERIAL_CLOCK_POLARITY_HIGH
                                | SERIAL_CLOCK_PHASE_START | DATA_FRAME_8BIT); // 8bit data frame size, CPOL=1,CPHA=1
        //CPOL=1,CPHA=1:空闲为高电平，第二个跳变沿采样

        //先设完spi的再设iomux，否则SPI-CLK会有一个低脉冲

        #if 1//SPIIomux();
        //spi_rxd_p1, spi_txd_p1, spi_clk_p1, spi_csn1
        GRFReg->GPIO0B_IOMUX = 1<<4 | 1<<6 | 1<<8 | 3<<20| 3<<22 | 3<<24;

        //GRFReg->GPIO0C_IOMUX = 0<<2|3<<18; //使用GPIOC[1]做SPI1的CS
        GpioMuxSet(GPIOPortC_Pin1,IOMUX_GPIOC1_IO);

        //spi0 切到gpio避免影响到 spi1 接口
        GRFReg->GPIO0A_IOMUX = 0<<6 | 0<<8 | 0<<10 | 0<<12 | 3<<22| 3<<24| 3<<26 | 3<<28;

        GRFReg->IOMUX_CON1 = 1<<1 | 1<<17; //spi_xxx_p1 available for spi 此设置只影响miso
        MPU6500_SPICsn = 2;
        //拉低SPI1 CS片选有效
        //GpioMuxSet(GPIOPortC_Pin1,IOMUX_GPIOC1_IO);
        Gpio_SetPinDirection(GPIOPortC_Pin1,GPIO_OUT);
        Gpio_SetPinLevel(GPIOPortC_Pin1, GPIO_LOW);//SPI1_CS
        #endif
    }

}

/*
Name:       MPU6500_RegConfig_Init
Desc:
Param:
Return:
Global:
Note:
Author:
Log:
*/
_ATTR_FLASH_CODE_
static void MPU6500_RegConfig_Init(void)
{
    MPU6500_Write(PWR_MGMT_1_REG,0X80);   		//电源管理,复位MPU6500
    DelayMs(100);
	MPU6500_Write(SIGNAL_PATH_RESET_REG,0X07);  //陀螺仪、加速度计、温度计复位
	DelayMs(100);

    MPU6500_Write(PWR_MGMT_1_REG,0X01);   		//选择时钟源
	MPU6500_Write(PWR_MGMT_2_REG,0X00);   		//使能加速度计和陀螺仪
	MPU6500_Write(CONFIG_REG,0X02);				//低通滤波器 0x02 92hz (3.9ms delay) fs=1khz
	MPU6500_Write(SMPLRT_DIV_REG,0X00);			//采样率1000/(1+0)=1000HZ
	MPU6500_Write(GYRO_CONFIG_REG,0X18);  		//陀螺仪测量范围 0X18 正负2000度
	MPU6500_Write(ACCEL_CONFIG_REG,0x00); 		//加速度计测量范围 0X00 正负2g(0X10 +/- 8g)
	MPU6500_Write(ACCEL_CONFIG2_REG,0x00);		//加速度计速率1khz 滤波器460hz (1.94ms delay)
}

/*
Name:       MPU6500_Read_Temperature
Desc:
Param:
Return:
Global:
Note:
Author:
Log:
*/
_ATTR_FLASH_CODE_
float MPU6500_Read_Temperature(uint8 *tempeture)
{
    uint8 mpu6500_Temperature_l = 0;
    uint8 mpu6500_Temperature_h = 0;

    int16 mpu6500_tempreature_temp;
    float mpu6500_tempreature = 0;

    MPU6500_Read(TEMP_OUT_L_REG|0x80, &mpu6500_Temperature_l, 1);
    MPU6500_Read(TEMP_OUT_H_REG|0x80, &mpu6500_Temperature_h, 1);

    tempeture[0] = mpu6500_Temperature_l;
    tempeture[1] = mpu6500_Temperature_h;


    mpu6500_tempreature_temp = mpu6500_Temperature_h;
    mpu6500_tempreature_temp = (mpu6500_tempreature_temp<<8)|mpu6500_Temperature_l;
    mpu6500_tempreature	=	(float)(35000+((521+mpu6500_tempreature_temp)*100)/34); // 原来分母为340，现在分子*100，即：扩大1000倍；
	mpu6500_tempreature = mpu6500_tempreature/1000;

    //printf ("mpu6500_tempreature=%f\n",mpu6500_tempreature/2);
    return mpu6500_tempreature;
}


/*
Name:       MPU6500_Read_Data
Desc:
Param:
Return:
Global:
Note:
Author:
Log:
*/
_ATTR_FLASH_CODE_
int32 MPU6500_Read_Data(int16 *d_accel, uint8 r_accel, int16 *d_gyro, uint8 r_gyro)
{

    uint8 mpu6500_data_l = 0;
    uint8 mpu6500_data_h = 0;
    uint16 mpu6500_accel_data[3]={0};
    int16 mpu6500_gyro_data[3]={0};
    int8 gyro_data_l = 0;
    int8 gyro_data_h = 0;

    if (r_accel == 1)
    {
        MPU6500_Read(ACCEL_XOUT_L_REG|0x80, &mpu6500_data_l, 1);
        MPU6500_Read(ACCEL_XOUT_H_REG|0x80, &mpu6500_data_h, 1);
        mpu6500_accel_data[0] = mpu6500_data_h;
        mpu6500_accel_data[0] = (mpu6500_accel_data[0]<<8)|mpu6500_data_l;
        MPU6500_Read(ACCEL_YOUT_L_REG|0x80, &mpu6500_data_l, 1);
        MPU6500_Read(ACCEL_YOUT_H_REG|0x80, &mpu6500_data_h, 1);
        mpu6500_accel_data[1] = mpu6500_data_h;
        mpu6500_accel_data[1] = (mpu6500_accel_data[1]<<8)|mpu6500_data_l;
        MPU6500_Read(ACCEL_ZOUT_L_REG|0x80, &mpu6500_data_l, 1);
        MPU6500_Read(ACCEL_ZOUT_H_REG|0x80, &mpu6500_data_h, 1);
        mpu6500_accel_data[2] = mpu6500_data_h;
        mpu6500_accel_data[2] = (mpu6500_accel_data[2]<<8)|mpu6500_data_l;


        d_accel[0] = mpu6500_accel_data[0];
        d_accel[1] = mpu6500_accel_data[1];
        d_accel[2] = mpu6500_accel_data[2];


    }

    if (r_gyro == 1)
    {
        MPU6500_Read(GYRO_XOUT_L_REG|0x80, &mpu6500_data_l, 1);
        MPU6500_Read(GYRO_XOUT_H_REG|0x80, &mpu6500_data_h, 1);
        //printf("gyro_xl=0x%x gyro_xh=0x%x\n",mpu6500_data_l,mpu6500_data_h);
        mpu6500_gyro_data[0]= mpu6500_data_h;
        mpu6500_gyro_data[0]= (mpu6500_gyro_data[0]<<8)|mpu6500_data_l;

        MPU6500_Read(GYRO_YOUT_L_REG|0x80, &mpu6500_data_l, 1);
        MPU6500_Read(GYRO_YOUT_H_REG|0x80, &mpu6500_data_h, 1);
        //printf("gyro_yl=0x%x gyro_yh=0x%x\n",mpu6500_data_l,mpu6500_data_h);
        mpu6500_gyro_data[1]= mpu6500_data_h;
        mpu6500_gyro_data[1]= (mpu6500_gyro_data[1]<<8)|mpu6500_data_l;

        MPU6500_Read(GYRO_ZOUT_L_REG|0x80, &mpu6500_data_l, 1);
        MPU6500_Read(GYRO_ZOUT_H_REG|0x80, &mpu6500_data_h, 1);
        //printf("gyro_zl=0x%x gyro_zh=0x%x\n",mpu6500_data_l,mpu6500_data_h);
        mpu6500_gyro_data[2]= mpu6500_data_h;
        mpu6500_gyro_data[2]= (mpu6500_gyro_data[2]<<8)|mpu6500_data_l;

        d_gyro[0] = mpu6500_gyro_data[0];
        d_gyro[1] = mpu6500_gyro_data[1];
        d_gyro[2] = mpu6500_gyro_data[2];

    }
    #if 0
    printf ("accel: %d %d %d  gyro: %d %d %d\n",
             d_accel[0],
             d_accel[1],
             d_accel[2],
             d_gyro[0],
             d_gyro[1],
             d_gyro[2]);
    #endif
    return 0;
}


/*
Name:       MPU6500_Init
Desc:
Param:
Return:
Global:
Note:
Author:
Log:
*/
_ATTR_FLASH_CODE_
int32 MPU6500_Init(void)
{
    int i =0;
    int32 ret = ERROR;
    uint8 mpu6500_id = 0;

    MPU6500_Spi_Init(SPI_PORT0, 2);//1:spi flash; 2:MPU6500

    //Read MPU6500 ID
    ret = MPU6500_Read(WHO_AM_I_REG|0x80, &mpu6500_id, 1);
    if (ret == OK)
    {
        printf ("mpu6500_id=0x%x\n",mpu6500_id);
    }
    else
    {
        printf ("Read WHO_AM_I_REG timeout\n");
    }

    MPU6500_RegConfig_Init();

    return ret;
}





